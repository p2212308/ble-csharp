﻿using System;
using System.Net;
using System.Net.Sockets;
using static System.Net.Sockets.SocketTaskExtensions;
using System.Threading;
using BLE_Protocol.Converter;
using BLE_Protocol.Datagramme;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BLE_Protocol.TCP
{
    public class TCPClient
    {
        private Socket _socket;

        private int _port;

        private IPEndPoint _endpoint;

        public TCPClient(int port)
        {
            this._port = port;
            _endpoint = new IPEndPoint(new IPAddress(new byte[] { 127,0,0,1} ), _port);
            Connect();
        }
        
        /// <summary>
        /// Méthode permettant de se connecter en TCP
        /// </summary>
        public async void Connect()
        {
            try
            {
                // Ce nombre représente l'adresse IPv4 du localhost en long
                _socket = new Socket(_endpoint.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
                Console.WriteLine("IP : " + _endpoint.Address);
                Console.WriteLine("AddressFamily : " + _endpoint.AddressFamily);

                await _socket.ConnectAsync(_endpoint);

                Thread thread = new Thread(WriteMessage);
                thread.Start();
            }
            catch(Exception e)
            {
                
            }

        }

        public async void WriteMessage()
        {
            var elements = DatagrammeBuilder.BuildFrameMessageForTCP(MessageManager.Messages.ToArray());

            while (elements.Count != 0)
            {
                foreach (var element in elements)
                {
                    await _socket.SendAsync(new ArraySegment<byte>(element), SocketFlags.None);
                }

                elements.Clear();

                await Task.Delay(TimeSpan.FromSeconds(5));
            }

            _socket.Close((int)TimeSpan.FromSeconds(5).TotalSeconds);
        }
    }
}