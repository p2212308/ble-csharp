﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLE_Protocol.Interface
{
    public interface IDevice
    {
        string Name { get; set; }

        ulong Address { get; set; }

        bool IsConnected { get; set; }
    }
}
