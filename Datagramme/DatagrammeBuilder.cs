﻿using System;
using System.Collections.Generic;
using BLE_Protocol.Converter;

namespace BLE_Protocol.Datagramme
{
    public class DatagrammeBuilder
    {
        /// <summary>
        /// Méthode permettant de construire le datagramme
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public static List<byte[]> BuildFrameMessage(params String[] list)
        {
            byte[] entete = { 0x77, 0x61, 0x6E, 0x67, 0x00, 0x00, 0x00, 0x00, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30 };
            byte[] longeurMsg = DataToByteArray.MsgLength(list);
            
            //Partie Date
            byte[] sixZeroslines = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
            byte[] date = BitConverter.GetBytes(DateTime.Now.ToBinary());
            
            byte[] sixBytesArray = new byte[10];
            Array.Copy(date, sixBytesArray, 6);
            
            sixBytesArray[6] = 0x00;
            sixBytesArray[7] = 0x00;
            sixBytesArray[8] = 0x00;
            sixBytesArray[9] = 0x00;
            //Fin Date
            
            //Fillers avant le packet de données
            byte[] fullzeros = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
            
            List<byte[]> elements = new List<byte[]>() { entete, longeurMsg, sixZeroslines, sixBytesArray, fullzeros };
            
            //Partie écriture des données
            for (int i = 0; i < list.Length; i++)
            {
                elements.AddRange(DataToByteArray.StringContentToByteArray(list[i]));
            }

            return elements;
        }

        /// <summary>
        /// Méthode permettant de construire le datagramme
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public static List<byte[]> BuildFrameMessageForTCP(params String[] list)
        {
            List<byte[]> elements = new List<byte[]>(){
                { new byte[] { /*0x00, 0x38, 0x6C, 0xC6, 0xC6, 0xFE, 0xC6, 0xC6, 0xC6, 0xC6, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00*/ 0x05,0x06,0x02} },
                //{ new byte[] { 0x00, 0x7C, 0xC6, 0xCE, 0xDE, 0xF6, 0xE6, 0xC6, 0xC6, 0x7C, 0x00 } }
            };

            //Partie écriture des données
            /*for (int i = 0; i < list.Length; i++)
            {
                elements.AddRange(DataToByteArray.StringContentToByteArray(list[i]));
                Console.WriteLine(list[i]);
            }*/

            return elements;
        }
    }
}