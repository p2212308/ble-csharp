﻿using BLE_Protocol.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLE_Protocol.Scanner
{
    public class ScanDevice : IDevice
    {
        public String Name { get; set; }

        public bool IsConnected { get; set; }

        public ulong Address { get; set; }

        public ScanDevice(string name, bool isConnected, ulong Address)
        {
            this.Name = name;
            this.IsConnected = isConnected;
            this.Address = Address;
        }

    }
}
