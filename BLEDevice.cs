﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Devices.Bluetooth;
using Windows.Devices.Bluetooth.GenericAttributeProfile;

namespace BLE_Protocol
{
    public class BLEDevice
    {
        public BluetoothLEDevice BluetoothDevice { get; set; }

        public GattCharacteristic BLEWriter { get; set; }

        public BLEDevice(BluetoothLEDevice device, GattCharacteristic characteristic) 
        { 
            this.BluetoothDevice = device;
            this.BLEWriter = characteristic;
        }
    }
}
