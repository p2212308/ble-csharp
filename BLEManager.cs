﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using BLE_Protocol.Interface;
using Windows.Devices.Bluetooth;
using Windows.Devices.Bluetooth.Advertisement;
using Windows.Devices.Bluetooth.Rfcomm;
using Windows.Devices.Bluetooth.GenericAttributeProfile;
using Windows.Networking.Sockets;
using BLE_Protocol.Scanner;
using Windows.Storage.Streams;
using System.Runtime.InteropServices.WindowsRuntime;
using BLE_Protocol.Converter;
using BLE_Protocol.Datagramme;

namespace BLE_Protocol
{
    public class BLEManager
    {
        public static Guid UUID_CHARACTERISTICS_WRITE = Guid.Parse("0000fee1-0000-1000-8000-00805f9b34fb");

        public static Guid UUID_SERVICE = Guid.Parse("0000fee0-0000-1000-8000-00805f9b34fb");

        /// <summary>
        /// Liste qui récupère les appareils bluetooth en utilisant le ScanDevice
        /// <seealso cref="https://inthehand.github.io/html/M_InTheHand_Bluetooth_Bluetooth_ScanForDevicesAsync.htm"/>
        /// </summary>
        public List<BLEDevice> Devices { get; private set; }

        public event EventHandler<IDevice> OnDiscoverDeviceNotification;

        public event EventHandler<ulong> OnDeviceNotDiscoveredNotification;

        public static event EventHandler<String> OnMessagePublishing;

        public bool IsScanningOff { get; set; }

        public string PrefixFilter { get; set; } = String.Empty;
        
        public List<String> MessagesToSend { get; set; }

        public BLEManager()
        {
            Devices = new List<BLEDevice>();
            MessagesToSend = new List<string>();
            IsScanningOff = false;

            OnMessagePublishing += BLEManager_OnMessagePublishing;

            BluetoothLEAdvertisementWatcher watcher = new BluetoothLEAdvertisementWatcher();
            watcher.Received += Watcher_Received;
            watcher.Start();


        }

        private void BLEManager_OnMessagePublishing(object sender, string e)
        {
            MessagesToSend.Add(e);

            foreach (var device in Devices)
                CharacteristicsWrite(device);
        }

        private async void Watcher_Received(BluetoothLEAdvertisementWatcher sender, BluetoothLEAdvertisementReceivedEventArgs args)
        {
            BluetoothLEDevice device = await BluetoothLEDevice.FromBluetoothAddressAsync(args.BluetoothAddress);

            if(device != null && device.Name.StartsWith(PrefixFilter))
            {
                ScanDevice scanDevice = new ScanDevice(device.Name, device.ConnectionStatus == BluetoothConnectionStatus.Connected, args.BluetoothAddress);

                if (!Devices.Any(i => i.BluetoothDevice.BluetoothAddress.Equals(args.BluetoothAddress)))
                {
                    var services = await device.GetGattServicesAsync();

                    GattCharacteristic characteristic = null;

                    if (services.Status == GattCommunicationStatus.Success)
                    {
                        var service = services.Services.FirstOrDefault(i => i.Uuid == UUID_SERVICE);
                        var characteristicResult = await service.GetCharacteristicsForUuidAsync(UUID_CHARACTERISTICS_WRITE);

                        characteristic = characteristicResult.Characteristics.FirstOrDefault(i => i.Uuid == UUID_CHARACTERISTICS_WRITE);
                    }

                    Devices.Add(new BLEDevice(device, characteristic));
                    OnDiscoverDeviceNotification?.Invoke(this, (IDevice)scanDevice);
                }
            }
                
            await Task.Delay(TimeSpan.FromSeconds(5));
        }

        public async Task<bool> ConnectTo(IDevice deviceBLE)
        {

            BluetoothLEDevice device = await BluetoothLEDevice.FromBluetoothAddressAsync(deviceBLE.Address);

            BLEDevice bLEDevice = Devices.FirstOrDefault(i => i.BluetoothDevice.BluetoothAddress == deviceBLE.Address);

            if (device == null)
                return false;

            CharacteristicsWrite(bLEDevice);

            return true;
        }

        public async void CharacteristicsWrite(BLEDevice device)
        {
            if (device.BLEWriter == null)
                return;

            using (var writer = new DataWriter())
            {
                foreach(var element in DatagrammeBuilder.BuildFrameMessage(MessagesToSend.ToArray()))
                {
                    writer.WriteBytes(element);
                    IBuffer buffer = writer.DetachBuffer();

                    var result = await device.BLEWriter.WriteValueWithResultAsync(buffer);
                    Console.WriteLine(result.Status);
                }
            }
        }

        public static void InvokeMessagePublishing(String item)
        {
            OnMessagePublishing.Invoke(null, item);
        }
    }
}
